<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prize_types}}`.
 */
class m200131_000813_create_prize_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%prize_types}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%prize_types}}');
    }
}
