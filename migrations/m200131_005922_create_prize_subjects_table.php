<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prize_subjects}}`.
 */
class m200131_005922_create_prize_subjects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%prize_subjects}}', [
            'id' => $this->primaryKey(),
            "name" => $this->string()->notNull(),
            "points" => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%prize_subjects}}');
    }
}
