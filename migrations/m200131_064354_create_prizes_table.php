<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prizes}}`.
 */
class m200131_064354_create_prizes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('prizes', [
            'id' => $this->primaryKey(),
            'amount' => $this->integer()->null(),
            'type_id' => $this->integer()->null(),
            'subject_id' => $this->integer()->null(),
        ]);

        $this->createIndex(
            'idx-prizes-type_id',
            'prizes',
            'type_id'
        );

        $this->createIndex(
            'idx-prizes-subject_id',
            'prizes',
            'subject_id'
        );

        $this->addForeignKey(
            'fk-prizes-subject_id',
            'prizes',
            'subject_id',
            'prize_subjects',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-prizes-type_id',
            'prizes',
            'type_id',
            'prize_types',
            'id',
            'RESTRICT'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex("idx-prizes-type_id","prizes");
        $this->dropIndex("idx-prizes-subject_id","prizes");
        $this->dropForeignKey("fk-prizes-type_id","prizes");
        $this->dropForeignKey("fk-prizes-subject_id","prizes");
        $this->dropTable('{{%prizes}}');
    }
}
