<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

/**
 * @var \app\models\Prize $prize
 */
$this->title = 'Поздравляем вы выиграли приз! - ' . $prize->subject->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <a href="<?=\yii\helpers\Url::toRoute(['prize/convert'])?>&id=<?=$prize->id ?>" class="btn btn-info btn-lg">
        Конвертировать в бонусные балы
    </a>
    <button class="btn btn-info btn-lg">Получить по почте</button>
</div>
