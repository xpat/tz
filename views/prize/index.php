<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Розыгрыш призов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <form method="post">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <button class="btn btn-primary btn-lg" type="submit">Получить приз</button>
    </form>
</div>
