<?php

/* @var $this yii\web\View */

use app\models\Prize;
use yii\helpers\Html;

/**
 * @var Prize $prize
 */
$this->title = 'Поздравляем вы выиграли бонусные балы! - ' . $prize->amount;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <button class="btn btn-info btn-lg">Просто порадоваться</button>
</div>
