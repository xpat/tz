<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

/**
 * @var integer $points
 */
$this->title = "В ваш аккаунт зачислены {$points} баллов!";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
