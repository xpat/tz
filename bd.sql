-- MariaDB dump 10.17  Distrib 10.4.6-MariaDB, for osx10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: slotegrator
-- ------------------------------------------------------
-- Server version	10.4.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1580429764),('m200131_000813_create_prize_types_table',1580432969),('m200131_005922_create_prize_subjects_table',1580432969),('m200131_064354_create_prizes_table',1580432969);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prize_subjects`
--

DROP TABLE IF EXISTS `prize_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prize_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prize_subjects`
--

LOCK TABLES `prize_subjects` WRITE;
/*!40000 ALTER TABLE `prize_subjects` DISABLE KEYS */;
INSERT INTO `prize_subjects` VALUES (1,'Iphone',1000),(2,'MackBook air',5000),(3,'MacBook pro',9000);
/*!40000 ALTER TABLE `prize_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prize_types`
--

DROP TABLE IF EXISTS `prize_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prize_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prize_types`
--

LOCK TABLES `prize_types` WRITE;
/*!40000 ALTER TABLE `prize_types` DISABLE KEYS */;
INSERT INTO `prize_types` VALUES (1,'Бонусные баллы'),(2,'Деньги'),(3,'Физический предмет');
/*!40000 ALTER TABLE `prize_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prizes`
--

DROP TABLE IF EXISTS `prizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-prizes-type_id` (`type_id`),
  KEY `idx-prizes-subject_id` (`subject_id`),
  CONSTRAINT `fk-prizes-subject_id` FOREIGN KEY (`subject_id`) REFERENCES `prize_subjects` (`id`),
  CONSTRAINT `fk-prizes-type_id` FOREIGN KEY (`type_id`) REFERENCES `prize_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prizes`
--

LOCK TABLES `prizes` WRITE;
/*!40000 ALTER TABLE `prizes` DISABLE KEYS */;
INSERT INTO `prizes` VALUES (2,3073,2,NULL),(3,4307,2,NULL),(4,1766,2,NULL),(5,774,2,NULL),(6,1917,2,NULL),(7,2515,2,NULL),(8,386,2,NULL),(9,3285,2,NULL),(10,1332,2,NULL),(11,3958,2,NULL),(12,2161,2,NULL),(13,1838,2,NULL),(14,4005,2,NULL),(15,3234,2,NULL),(16,1550,2,NULL),(17,1909,1,NULL),(18,NULL,3,2),(19,NULL,3,3),(20,NULL,3,1),(21,NULL,3,3),(22,NULL,3,2),(23,4525,2,NULL),(24,NULL,3,2),(25,NULL,3,2),(26,4986,1,NULL),(27,1789,2,NULL),(28,357,1,NULL),(29,4885,2,NULL),(30,1091,2,NULL),(31,NULL,3,2),(32,4988,1,NULL),(33,1271,2,NULL);
/*!40000 ALTER TABLE `prizes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-31 14:21:18
