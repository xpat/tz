<?php

namespace tests\unit\models\prize;

use app\models\Prize;
use app\models\prize\money\MoneyPrizeConverter;

class MoneyPrizeConverterTest extends  \Codeception\Test\Unit
{
    public function testConvertToPoints()
    {
        $converter = new MoneyPrizeConverter();

//        $mock = $this->createMock(Prize::class);
//        $mock->method('__get')
//            ->with('amount')
//            ->willReturn(10);

        $prize = new Prize();
        $prize->amount = 10;

        expect_that($converter->convertToPoints($prize) == 15);

        $prize->amount = 20;
        $converter->setCoefficient(3);
        expect_that($converter->convertToPoints($prize) == 60);
    }
}