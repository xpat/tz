<?php


namespace app\models;


use yii\db\ActiveRecord;

class PrizeType extends ActiveRecord
{
    const POINTS = 1;
    const MONEY = 2;
    const SUBJECT = 3;

    public static function tableName()
    {
        return '{{prize_types}}';
    }

    public static function getRandom() : PrizeType
    {
        $types = PrizeType::find()->all();

        return $types[rand(0,count($types) - 1)];
    }
}