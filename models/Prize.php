<?php


namespace app\models;

use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsTrait;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;


use yii\db\ActiveRecord;

class Prize extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::class,
                'relations' => [
                    'type',
                    'subject'
                ],
            ],
        ];
    }


    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function getType()
    {
        return $this->hasOne(PrizeType::class,["id" => "type_id"]);
    }

    public function getSubject()
    {
        return $this->hasOne(PrizeSubject::class,["id" => "subject_id"]);
    }

    public static function tableName()
    {
        return '{{prizes}}';
    }
    
    
}