<?php


namespace app\models;


use yii\db\ActiveRecord;

class PrizeSubject extends ActiveRecord
{

    public static function getRandom()
    {
        $subjects = PrizeSubject::find()->all();
        return $subjects[rand(0,count($subjects)-1)];
    }

    public static function tableName()
    {
        return '{{prize_subjects}}';
    }
}