<?php


namespace app\models\prize;


use app\models\prize\money\MoneyPrizeManager;
use app\models\prize\point\PointPrizeManager;
use app\models\prize\subject\SubjectPrizeManager;
use app\models\PrizeType;

class PrizeManagerFactory
{
    private $managers;

    public function __construct(
        MoneyPrizeManager $moneyPrizeManager,
        SubjectPrizeManager $subjectPrizeManager,
        PointPrizeManager $pointPrizeManager
    )
    {
        $this->managers = [
            PrizeType::MONEY => $moneyPrizeManager,
            PrizeType::POINTS => $pointPrizeManager,
            PrizeType::SUBJECT => $subjectPrizeManager,
        ];
    }

    public function getManager($type) : PrizeManagerInterface
    {
        if (isset($this->managers[$type])) {
            return $this->managers[$type];
        }

        throw new \Exception("Prize manager for type '$type' not found");
    }

}