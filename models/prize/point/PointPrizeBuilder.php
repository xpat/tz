<?php

namespace app\models\prize\point;

use app\models\Prize;
use app\models\prize\PrizeBuilderInterface;

class PointPrizeBuilder implements PrizeBuilderInterface
{
    protected $min = 100;
    protected $max = 5000;

    public function build($type): Prize
    {
        $prize = new Prize();
        $prize->amount = rand($this->min,$this->max);
        $prize->type = $type;
        $prize->save();
        return $prize;
    }
}