<?php


namespace app\models\prize\point;


use app\models\prize\PrizeViewInterface;

class PointPrizeView implements PrizeViewInterface
{

    public function getTemplate()
    {
        return "point-view";
    }
}