<?php


namespace app\models\prize\point;


use app\models\prize\PrizeBuilderInterface;
use app\models\prize\PrizeConverterInterface;
use app\models\prize\PrizeManagerInterface;
use app\models\prize\PrizeViewInterface;

class PointPrizeManager implements PrizeManagerInterface
{
    private $view;
    private $builder;

    public function __construct(PointPrizeView $view, PointPrizeBuilder $builder)
    {
        $this->view = $view;
        $this->builder = $builder;
    }

    public function getBuilder(): PrizeBuilderInterface
    {
        return $this->builder;
    }

    public function getConverter(): PrizeConverterInterface
    {
        throw new \Exception("converter not found");
    }

    public function getView(): PrizeViewInterface
    {
        return $this->view;
    }
}