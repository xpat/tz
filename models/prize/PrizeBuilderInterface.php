<?php


namespace app\models\prize;


use app\models\Prize;

interface PrizeBuilderInterface
{

    public function build($type) : Prize;
}