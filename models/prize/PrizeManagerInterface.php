<?php


namespace app\models\prize;


use app\models\prize\PrizeBuilderInterface;

interface  PrizeManagerInterface
{
    public function getBuilder() : PrizeBuilderInterface;

    public function getConverter() : PrizeConverterInterface;

    public function getView() : PrizeViewInterface;

}