<?php


namespace app\models\prize;


interface PrizeViewInterface
{
    public function getTemplate();
}