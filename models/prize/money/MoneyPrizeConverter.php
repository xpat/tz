<?php


namespace app\models\prize\money;


use app\models\Prize;
use app\models\prize\PrizeConverterInterface;

class MoneyPrizeConverter implements PrizeConverterInterface
{

    protected $coefficient = 1.5;

    /**
     * @param mixed $coefficient
     */
    public function setCoefficient($coefficient): void
    {
        $this->coefficient = $coefficient;
    }

    public function convertToPoints(Prize $prize): int
    {

        return $this->coefficient * $prize->amount;
    }
}