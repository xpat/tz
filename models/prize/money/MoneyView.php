<?php


namespace app\models\prize\money;


use app\models\prize\PrizeViewInterface;

class MoneyView implements PrizeViewInterface
{

    public function getTemplate()
    {
        return "money-view";
    }
}