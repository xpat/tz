<?php


namespace app\models\prize\money;


use app\models\prize\PrizeBuilderInterface;
use app\models\prize\PrizeConverterInterface;
use app\models\prize\PrizeManagerInterface;
use app\models\prize\PrizeViewInterface;


class MoneyPrizeManager implements PrizeManagerInterface
{
    private $builder;
    private $moneyView;
    private $converter;

    public function __construct(
        MoneyPrizeBuilder $builder,
        MoneyView $moneyView,
        MoneyPrizeConverter $converter
    )
    {
        $this->builder = $builder;
        $this->moneyView = $moneyView;
        $this->converter = $converter;
    }

    public function getBuilder(): PrizeBuilderInterface
    {
        return  $this->builder;
    }

    public function getConverter(): PrizeConverterInterface
    {
        return $this->converter;
    }

    public function getView(): PrizeViewInterface
    {
        return $this->moneyView;
    }
}