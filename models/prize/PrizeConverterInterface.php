<?php


namespace app\models\prize;


use app\models\Prize;

interface PrizeConverterInterface
{
    public function convertToPoints(Prize $prize) : int;
}