<?php


namespace app\models\prize\subject;


use app\models\Prize;
use app\models\prize\PrizeConverterInterface;

class SubjectPrizeConverter implements PrizeConverterInterface
{

    public function convertToPoints(Prize $prize): int
    {
        return $prize->subject->points;
    }
}