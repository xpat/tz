<?php


namespace app\models\prize\subject;


use app\models\prize\PrizeBuilderInterface;
use app\models\prize\PrizeConverterInterface;
use app\models\prize\PrizeManagerInterface;
use app\models\prize\PrizeViewInterface;

class SubjectPrizeManager implements PrizeManagerInterface
{

    private $builder;
    private $view;
    private $converter;

    public function __construct(SubjectPrizeBuilder $builder, SubjectPrizeView $view, SubjectPrizeConverter $converter)
    {
        $this->builder = $builder;
        $this->converter = $converter;
        $this->view = $view;
    }

    public function getBuilder(): PrizeBuilderInterface
    {
        return $this->builder;
    }

    public function getConverter(): PrizeConverterInterface
    {
        return $this->converter;
    }

    public function getView(): PrizeViewInterface
    {
        return $this->view;
    }
}