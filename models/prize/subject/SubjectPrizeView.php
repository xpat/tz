<?php


namespace app\models\prize\subject;


use app\models\prize\PrizeViewInterface;

class SubjectPrizeView implements PrizeViewInterface
{

    public function getTemplate()
    {
        return "subject-view";
    }
}