<?php

namespace app\models\prize\subject;

use app\models\Prize;
use app\models\prize\PrizeBuilderInterface;
use app\models\PrizeSubject;

class SubjectPrizeBuilder implements PrizeBuilderInterface
{

    public function build($type): Prize
    {
        $subject = PrizeSubject::getRandom();

        $prize = new Prize();
        $prize->type = $type;
        $prize->subject = $subject;
        $prize->save();
        return $prize;
    }
}