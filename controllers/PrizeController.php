<?php


namespace app\controllers;


use app\models\Prize;
use app\models\prize\PrizeManagerFactory;
use app\models\prize\PrizeManagerInterface;
use app\models\PrizeType;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PrizeController extends Controller
{

    public function actionIndex()
    {
        $request = \Yii::$app->request;

        if ($request->getMethod() == "POST") {

            $type = PrizeType::getRandom();

            $manager = $this->getManager($type->id);

            $prize = $manager->getBuilder()->build($type->id);

            return $this->render($manager->getView()->getTemplate(),[
                "prize" => $prize
            ]);

        }

        return $this->render('index');
    }


    public function actionConvert()
    {
        $request = \Yii::$app->request;

        $prize = Prize::findOne($request->get('id'));

        if (!$prize) {
            throw new NotFoundHttpException();
        }

        $manager = $this->getManager($prize->type->id);


        return $this->render("convert",[
            "points" => $manager->getConverter()->convertToPoints($prize)
        ]);

    }

    protected function getManager($type) : PrizeManagerInterface
    {
        return \Yii::$container->get(PrizeManagerFactory::class)->getManager($type);
    }

}